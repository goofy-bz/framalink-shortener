# 1.1.1
* Fix: Remove console.* for mozilla build

# 1.1.0
* +: Support img tags
* Fix: Context menu for links

# 1.0.0
* +: Action button
* +: Context menu to shorten the page
* +: Addon icon

# 0.9.1
* i: Localization changes
* Fix: Description localization

# 0.9.0
* i: Initial version
